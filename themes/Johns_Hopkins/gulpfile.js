const path = require('path');
const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

elixir.config.css.autoprefix.options.browsers = ['last 4 versions'];

elixir.config.css.sass.folder = 'scss';

elixir.config.css.sass.pluginOptions = {
    includePaths: [
        'node_modules/bootstrap-sass/assets/stylesheets',
        'node_modules/normalize.css',
        'node_modules/animate.css',
        'resources/assets',
        'resources/views',
    ],
};

elixir.ready(() => {
    elixir.webpack.mergeConfig({
        resolve: {
            alias: {
                components: path.resolve(__dirname, 'resources/views/components/vue'),
            },
        },
    });
});

elixir(mix => {
    mix
        .webpack('app.js', 'js/app.js')
        .sass('app.scss', 'css/app.css')
    ;
});
