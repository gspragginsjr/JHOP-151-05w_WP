<?php
/**
*
* @package johns_hopkins_financial_aid
*
*/

include 'vendor/autoload.php';
include 'inc/timber.php';

function myplugin_tinymce_buttons($buttons)
 {
  //Remove blockquote, horizontal rule, and align buttons
  $remove = array('blockquote','hr','alignleft','aligncenter','alignright');

  return array_diff($buttons,$remove);
 }

add_filter('teeny_mce_buttons','myplugin_tinymce_buttons');

if ( ! function_exists( 'jhop_intermediate_image_sizes_advanced' ) ) :
function jhop_intermediate_image_sizes_advanced( $sizes ) {
    unset( $sizes['thumbnail'] );
    unset( $sizes['medium'] );
    unset( $sizes['medium_large'] );
    unset( $sizes['large'] );

    return $sizes;
}
endif;
add_filter( 'intermediate_image_sizes_advanced', 'jhop_intermediate_image_sizes_advanced' );

if ( ! function_exists( 'jhop_after_setup_theme' ) ) :
function jhop_after_setup_theme() {
    add_image_size( 'small_custom', 320, 320 );
    add_image_size( 'medium_custom', 512, 512 );
    add_image_size( 'medium_large_custom', 768, 768 );
    add_image_size( 'large_custom', 1024, 1024 );
    add_image_size( 'huge_custom', 1280, 1280 );
    add_image_size( 'massive_custom', 1600, 1600 );
    add_image_size( 'hero_large', 1920, 1080 );
    add_image_size( 'hero_small', 1920, 425, true );
    add_image_size( 'video_thumb', 158, 105, true );

    register_nav_menus( array(
        'main-nav_menu' => 'Main Navigation',
        'active-links_menu' => 'Active Links Menu',
        'footer-nav_menu' => 'Footer Nav Menu',
        'footer-bottom_menu' => 'Footer Bottom Menu'
    ) );
}
endif;
add_action( 'after_setup_theme', 'jhop_after_setup_theme' );

if ( ! function_exists( 'jhop_init' ) ) :
function jhop_init() {
    register_post_type( 'Events', array(
        'labels'        => array (
            'name'               => __( 'Events' ),
            'singular_name'      => __( 'Event' ),
            'add_new_item'       => __( 'Add New Event' ),
            'edit_item'          => __( 'Edit Event' ),
            'new_item'           => __( 'New Event' ),
            'view_item'          => __( 'View Event' ),
            'search_items'       => __( 'Search Events' ),
            'not_found'          => __( 'No events found.' ),
            'not_found_in_trash' => __( 'No events found in trash' ),
        ), 'public'     => true,
        'menu_position' => 20,
        'menu_icon'     => 'dashicons-calendar-alt',
        'show_in_rest'  => true,
        'rest_base'     => 'events',
        'supports'      => array( 'title' ),
    ) );

    register_post_type( 'Testimonials', array(
        'labels'        => array (
            'name'               => __( 'Testimonials' ),
            'singular_name'      => __( 'Testimonial' ),
            'add_new_item'       => __( 'Add New Testimonial' ),
            'edit_item'          => __( 'Edit Testimonial' ),
            'new_item'           => __( 'New Testimonial' ),
            'view_item'          => __( 'View Testimonial' ),
            'search_items'       => __( 'Search Testimonials' ),
            'not_found'          => __( 'No testimonials found.' ),
            'not_found_in_trash' => __( 'No testimonials found in trash' ),
        ), 'public'     => true,
        'menu_position' => 20,
        'menu_icon'     => 'dashicons-testimonial',
        'show_in_rest'  => true,
        'rest_base'     => 'testimonials',
        'supports'      => array( 'title' ),
    ) );

    register_post_type( 'Stories', array(
        'labels'        => array (
            'name'               => __( 'Stories' ),
            'singular_name'      => __( 'Story' ),
            'add_new_item'       => __( 'Add New Story' ),
            'edit_item'          => __( 'Edit Story' ),
            'new_item'           => __( 'New Story' ),
            'view_item'          => __( 'View Story' ),
            'search_items'       => __( 'Search Stories' ),
            'not_found'          => __( 'No stories found.' ),
            'not_found_in_trash' => __( 'No stories found in trash' ),
        ), 'public'     => true,
        'menu_position' => 20,
        'menu_icon'     => 'dashicons-id-alt',
        'show_in_rest'  => true,
        'rest_base'     => 'stories',
        'supports'      => array( 'title' ),
    ) );

    acf_add_options_page(array(
        'page_title'    => 'Global Site Content',
        'menu_title'    => 'Global Site Content',
        'menu_slug'     => 'global-site-content',
        'capability'    => 'edit_posts',
        'position'      => 41,
        'redirect'      => false,
        'icon_url'      => 'dashicons-admin-site'
    ));

    acf_add_options_page(array(
        'page_title'    => 'General Instructions',
        'menu_title'    => 'General Instructions',
        'menu_slug'     => 'general-instructions',
        'capability'    => 'edit_posts',
        'position'      => 40,
        'redirect'      => false,
        'icon_url'      => 'dashicons-book'
    ));
}
endif;
add_action( 'init', 'jhop_init' );


function button_function($atts, $content = null) {
    extract(shortcode_atts(array(
          'url' => '#',
          'type' => 'solid',
       ), $atts));
    $return_string = '<a href="' . $url . '" class="btn_' . $type . '">' . $content . '</a>';

    return $return_string;
}

function register_shortcodes(){
    add_shortcode('button', 'button_function');
}
add_action( 'init', 'register_shortcodes');

function add_admin_menu_separator($position) {

    global $menu;
    $index = 0;

    foreach($menu as $offset => $section) {
        if (substr($section[2],0,9)=='separator')
            $index++;
        if ($offset>=$position) {
            $menu[$position] = array('','read',"separator{$index}",'','wp-menu-separator');
            break;
        }
    }

    ksort( $menu );
}

function admin_menu_separator() {
    add_admin_menu_separator(39);
}
add_action('admin_menu','admin_menu_separator');

Routes::map('form/:id', function($params){
    $query = 'pagename=campus-visit-form';
    Routes::load('calendar_form.php', $params, $query, 200);
});

function remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
    remove_menu_page( 'edit.php' );
}
add_action( 'admin_menu', 'remove_admin_menus' );
