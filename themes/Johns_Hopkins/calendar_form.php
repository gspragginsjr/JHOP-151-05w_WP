<?php
$context = Timber::get_context();
$context['form_id'] = $params['id'];

Timber::render( 'pages/calendar_form.twig', $context );
