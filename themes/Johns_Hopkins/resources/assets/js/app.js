import Vue from 'vue';
import Velocity from 'velocity-animate';
import svg4everybody from 'svg4everybody';
import objectFitImages from 'object-fit-images';
import anime from 'animejs'
// import WOW from 'wowjs';

import GlobalHeaderFinaid from 'components/GlobalHeaderFinaid';
import GlobalHeaderAdmissions from 'components/GlobalHeaderAdmissions';
import GlobalHeroSlider from 'components/GlobalHeroSlider';
import GlobalHeroSlide from 'components/GlobalHeroSlide';
import GlobalAccordionPanel from 'components/GlobalAccordionPanel';
import GlobalAccordion from 'components/GlobalAccordion';
import GlobalTableStatistics from 'components/GlobalTableStatistics';
import GlobalTableTwoCol from 'components/GlobalTableTwoCol';
import Gallery from 'components/Gallery';
import Modal from 'components/Modal';
import GlobalFormSlate from 'components/GlobalFormSlate';

svg4everybody();
objectFitImages();

new Vue({
    el: '#app',
    components: {
        GlobalHeaderFinaid,
        GlobalHeaderAdmissions,
        GlobalHeroSlider,
        GlobalHeroSlide,
        GlobalAccordionPanel,
        GlobalAccordion,
        GlobalTableStatistics,
        GlobalTableTwoCol,
        Gallery,
        Modal,
        GlobalFormSlate,
    },
});

new WOW().init();

var lineDrawing = anime({
  targets: '#pattern path',
  strokeDashoffset: [anime.setDashoffset, 0],
  easing: 'easeInOutSine',
  duration: 1500,
  delay: function(el, i) { return i * 250 },
  direction: 'alternate',
  loop: true
});

var lineDrawing2 = anime({
  targets: '#pattern2 path',
  strokeDashoffset: [anime.setDashoffset, 0],
  easing: 'easeInOutQuad',
  duration: 3000,
  delay: function(el, i) { return i * 250 },
  direction: 'alternate',
  loop: true
});

window.onload = function() {
    jerryRigHeroMargin();
};

window.onresize = function() {  
    jerryRigHeroMargin();
};

function jerryRigHeroMargin() {
    if(document.getElementById('textWrap') && window.innerWidth >= 767) {
        var finText = document.getElementById('textWrap');
        var finHero = document.getElementById('hero');
        var finText = document.getElementById('textWrap');
        var finTextHeight = finText.offsetHeight;

        var finMargin = finHero.style.marginBottom = finTextHeight - 100 + 120 + "px";

    } else if(document.getElementById('hero')) {
        document.getElementById('hero').style.cssText = "";
    }
}
