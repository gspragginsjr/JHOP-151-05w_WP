<?php

if ( ! function_exists( 'jhop_fin_aid_timber_context' ) ) :
function jhop_fin_aid_timber_context( $context ) {
    $context['post'] = Timber::get_post();
    $context['posts'] = Timber::get_posts();
    $context['main_nav_menu'] = new TimberMenu('main-nav_menu');
    $context['active_links_menu'] = new TimberMenu('active-links_menu');
    $context['footer_nav_menu'] = new TimberMenu('footer-nav_menu');
    $context['footer_bottom_menu'] = new TimberMenu('footer-bottom_menu');
    $context['options'] = get_fields('option');
    $context['the_search_query'] = get_search_query();
    $context['is_search'] = is_search();
    $context['calendar'] = calendar();
    $context['admin'] = is_user_logged_in();

    if ( $context['post'] ) {
        $context['breadcrumbs'] = get_post_ancestors( $context['post']->ID );

        if ($context['breadcrumbs']) {
            $context['top_ancestor'] = new TimberPost(array_values(array_slice($context['breadcrumbs'], -1))[0]);
        } else {
            $context['top_ancestor'] = Timber::get_post();
        }

    } else {
        $context['top_ancestor'] = Timber::get_post();
    }

    return $context;
}
endif;
add_filter( 'timber/context', 'jhop_fin_aid_timber_context' );

Timber::$dirname = 'resources/views';

function calendar() {
    $eventUUIDStore = array();

    $jsonEventsURL = 'https://admissions.jhu.edu/manage/query/run?id=da5c9d32-178b-43be-bf76-d3eab890d40c&h=60b8c9bb-5eaa-6dbd-e953-4ef20032a04b&cmd=service&output=json';
    $jsonEventsData = url_get_contents($jsonEventsURL);
    $jsonEventsData = json_decode($jsonEventsData);

    foreach ($jsonEventsData->row as $event) {
        $eventUUIDStore[str_replace("-","", $event->date)] = $event->id;
    }

    $return = '';

    $year = array();
    $year['2017'] = 12;
    $year['2018'] = 2;

    $flipbit = true;

    $calendar = array();

    date_default_timezone_set('America/New_York');
    for($m = date('Y'); $m <= 2017; $m++) {
      $startmonth = 1;
      if(date('m') <= $year[$m] && $m == date('Y')) {
        $startmonth = date('m');
      } else {
        $startmonth = 1;
      }
      for ($n = $startmonth; $n <= 8 ; $n++) {
        if($flipbit) {
            // $return .= '<div class="c"></div>';
            $flipbit = false;
        } else {
            $flipbit = true;
        }

        $return .= '<table>';
        $return .= '<tr><td colspan="7" class="month">'.date('F', mktime(0,0,0,$n,1,$m)).'</td></tr>
      <tr>
      <td>Su</td>
      <td>M</td>
      <td>Tu</td>
      <td>W</td>
      <td>Th</td>
      <td>F</td>
      <td>Sa</td>
      </tr>';

        for($i = 1; $i <= date('t', mktime(0,0,0,$n,1,$m)); $i++) {
          $wd = date('w', mktime(0, 0, 0, $n, $i, $m));
          if($i == 1) {
            $return .= '<tr>';
            for($j = 0; $j < $wd; $j++) {
                $return .= '<td></td>';
            }
          } else if ($wd == 0) {
            $return .= '<tr>';
          }
          $ds = date('Ymd', mktime(0, 0, 0, $n, $i, $m));
          if(!empty($eventUUIDStore[$ds])) {
            if(date('Ymd') > $ds) {
              $return .= '<td class="nt">'.date('d', mktime(0, 0, 0, $n, $i, $m)).'</td>';
            } else if (date('Ymd') == $ds) {
              $return .= '<td class="today">'.date('d', mktime(0, 0, 0, $n, $i, $m)).'</td>';
            } else if ($eventUUIDStore[$ds] == 'full') {
              $return .= '<td title="This day is at capacity. If you have not already registered for this day, please select another day to visit from our main calendar.">'.date('d', mktime(0, 0, 0, $n, $i, $m)).'</td>';
            } else if ($eventUUIDStore[$ds] == 'oh') {
              $return .= '<td><a href="/visit/openhouse">'.date('d', mktime(0, 0, 0, $n, $i, $m)).'</a></td>';
            } else {
              $return .= '<td class="available"><a href="/form/'.$eventUUIDStore[$ds].'/">'.date('d', mktime(0, 0, 0, $n, $i, $m)).'</a></td>';
            }
          } else {
            $return .= '<td class="nt">'.date('d', mktime(0, 0, 0, $n, $i, $m)).'</td>';
          }
          if($i == date('t', mktime(0,0,0,$n,1,$m))) {
            if($wd != 6) {
              for($j = ($wd+1); $j < 7; $j++) {
                  $return .= '<td></td>';
              }
            }
            $return .= '</tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
            $return .= '</table>';
          } else if ($wd == 6) {
            $return .= '</tr>';
          }
        }
      }
    }
    return $return;
}

function url_get_contents ($Url) {
    if (!function_exists('curl_init')){
        die('CURL is not installed!');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}
