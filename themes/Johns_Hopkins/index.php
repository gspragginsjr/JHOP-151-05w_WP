<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package johns_hopkins_financial_aid
 */

$context = Timber::get_context();

if ($context['is_search'] == true) {
    Timber::render( 'pages/search.twig', $context );
} elseif ($context['post']) {
    Timber::render( 'pages/page-without-sidebar.twig', $context );
} else {
    Timber::render( 'pages/404.twig', $context );
}
