# JHOP-151-05w / JHOP-151-04w Johns Hopkins University Sites

Package for JHOP's Admissions and Financial Aid sites. README details apply to both.

## Dependencies
+ Node.js
+ Webpack

## Install using NPM

```bash
$ npm install
```

## Process javascript, sass and any gulp tasks

```bash
$ gulp
```

## Watch javascript, sass and any gulp tasks

```bash
$ gulp watch
```

## JavaScript
Site is built with modular components using the Vue.js javascript framework, in tandem with [Timber](https://timber.github.io/timber/) Wordpress contexts and [twig](https://twig.sensiolabs.org/) templating syntax for PHP data display and retrieval.

### File structure
* `/themes/Johns_Hopkins/resources/views/components/` -- Base folder for all site component files. Any component utilizing javascript is further routed to a '.vue' counterpart found using the path below.
* `/themes/Johns_Hopkins/resources/views/components/vue` -- Vue component files.
* `/themes/Johns_Hopkins/resources/assets/js/app.js` -- Importing and indexing file for all vue components and other miscellaneous javascript tasks. Minified / production version found at `/themes/Johns_Hopkins/js/app.js`
